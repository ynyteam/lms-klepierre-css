#INSTALL

1. cd path/to/your/folder

3. npm install

4. npm install --global gulp-cli (si gulp cli n'a pas déjà été installé sur votre système)



#WORKFLOW

1. npm run start (lance la tache gulp sass:watch)

2. editer/créer la feuille de style adéquate pour la mise à jour du LMS.
(./scss/file.scss)

3. Cette feuille sera compilée et enregistrée dans le dossier ./css/file.css

4. Tester cette feuille avec l'extension Stylebot (Chrome)

5. Une fois le style validé, copier le dans l'éditeur proposé par le LMS


