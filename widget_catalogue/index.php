<?php
    $language = $_GET['lang'];
    $data = array (
        "fr" => array(
            "main_button_label" => 'DÉCOUVREZ TOUT<br>LE CATALOGUE DE FORMATIONS',
            "catalog_label" => "LE CATALOGUE",
            "calendar_label" => "LE CALENDRIER",
            "link_catalog" => "15/le-catalogue-des-formations",
            "link_calendar" => "17/le-calendrier-des-formationsfr"
        ),
        "en" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "29/entraining-catalog",
            "link_calendar" => "30/entraining-calendar"
        ),
        "es" => array(
            "main_button_label" => 'DESCUBRE EL CATÁLOGO<br>DE FORMACIÓN COMPLETO',
            "catalog_label" => "CATÁLOGO DE FORMACIÓN",
            "calendar_label" => "CALENDARIO DE FORMACIONES",
            "link_catalog" => "95/estraining-catalog",
            "link_calendar" => "96/estraining-calendar"
        ),
        "it" => array(
            "main_button_label" => 'SCOPRI IL<br>CATALOGO TRAINING',
            "catalog_label" => "CATALOGO TRAINING",
            "calendar_label" => "CALENDARIO TRAINING",
            "link_catalog" => "90/ittraining-catalog",
            "link_calendar" => "91/ittraining-calendar"
        ),
        "tr" => array(
            "main_button_label" => 'TÜM EĞİTİM<br>KATALOĞUNA GÖZAT',
            "catalog_label" => "EĞITIM KATALOĞU",
            "calendar_label" => "EĞITIM AJANDASI",
            "link_catalog" => "85/trtraining-catalog",
            "link_calendar" => "86/trtraining-calendar"
        ),
        "hu" => array(
            "main_button_label" => 'FEDEZD FEL A<br>TRÉNING KATALÓGUST!',
            "catalog_label" => "TRÉNING KATALÓGUS",
            "calendar_label" => "TRÉNING NAPTÁR",
            "link_catalog" => "80/hutraining-catalog",
            "link_calendar" => "81/hutraining-calendar"
        ),
        "pl" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "34/pltraining-catalog",
            "link_calendar" => "35/pltraining-calendar"
        ),
        "se" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "40/setraining-catalog",
            "link_calendar" => "41/setraining-calendar"
        ),
        "no" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "45/notraining-catalog",
            "link_calendar" => "46/notraining-calendar"
        ),
        "dk" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "50/dktraining-catalog",
            "link_calendar" => "51/dktraining-calendar"
        ),
        "cz" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "55/cztraining-catalog",
            "link_calendar" => "56/cztraining-calendar"
        ),
        "de" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "60/detraining-catalog",
            "link_calendar" => "61/detraining-calendar"
        ),
        "nl" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "65/nltraining-catalog",
            "link_calendar" => "66/nltraining-calendar"
        ),
        "pt" => array(
            "main_button_label" => 'DISCOVER ALL<br>THE TRAINING CATALOG',
            "catalog_label" => "TRAINING CATALOG",
            "calendar_label" => "TRAINING CALENDAR",
            "link_catalog" => "75/pttraining-catalog",
            "link_calendar" => "76/pttraining-calendar"
        )
    );
?>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="cache-control" content="no-cache">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
            <div class='wrapper'>
                    <a style='color: white' href='https://www.university.klepierre.com/pages/<?php echo $data[$language][link_catalog]; ?>' target='_parent'>
                        <div class='main_button'>
                            <h2><?php echo $data[$language][main_button_label]; ?></h2>
                        </div>
                    </a>
                    <div class='sub_buttons'>
                        <a style='color: white;' href='https://www.university.klepierre.com/pages/<?php echo $data[$language][link_catalog]; ?>' target='_parent'>
                            <div class='button' style='background-color: #32d1cd;'>
                                <div class='wrapper_button'>
                                        <div>
                                            <img style='max-width:83px;' src='https://remote.yesnyou.com/klepierre/lms-klepierre-css/widget_catalogue/images/catalogue.png'>
                                        </div>
                                        <div>
                                            <p style='font-size: 12px; margin-bottom: 0px;'><?php echo $data[$language][catalog_label]; ?></p>
                                        </div>
                                </div>
                            </div>
                        </a>
                        <a style='color: white;'  href='https://www.university.klepierre.com/pages/<?php echo $data[$language][link_calendar]; ?>' target='_parent'>
                            <div class='button' style='background-color: #71d1df;'>
                                <div class='wrapper_button'>
                                        <div>
                                            <img style='max-width:75px;' src='https://remote.yesnyou.com/klepierre/lms-klepierre-css/widget_catalogue/images/calendrier.png'>
                                        </div>
                                        <div>
                                            <p style='font-size: 12px; margin-bottom: 0px;'><?php echo $data[$language][calendar_label]; ?></p>
                                        </div>
                                </div>
                            </div>
                        </a>
                        
                    </div>
            </div>
    </body>
    
</html>

