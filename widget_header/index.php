<?php 
    include ('auth.php');
    $language = $_GET['lang'];
    $data = array (
        "fr" => array(
            "hello_message" => "Bonjour",
            "my_catalog_label" => "MON CATALOGUE",
            "my_activity_label" => "MON ACTIVITÉ",
            "my_formation_label" => "MES FORMATIONS",
            "my_training_path_label" => "MON PARCOURS",
            "see_more_label" => "VOIR",
            "contact_label" => "CONTACT",
            "link_catalog" => "15/le-catalogue-des-formationsfr",
            "link_my_training_path" => "18/mon-parcours"
        ),
        "en" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "29/entraining-catalog",
            "link_my_training_path" => "31/enmy-path"
        ),
        "es" => array(
            "hello_message" => "Hola",
            "my_catalog_label" => "MI CATÁLOGO",
            "my_activity_label" => "MI ACTIVIDAD",
            "my_formation_label" => "MI FORMACIÓN",
            "my_training_path_label" => "MI RUTA DE FORMACIONES",
            "see_more_label" => "DESCUBRE MÁS",
            "contact_label" => "CONTACTO",
            "link_catalog" => "95/estraining-catalog",
            "link_my_training_path" => "97/mi-ruta-de-formaciones"
        ),
        "it" => array(
            "hello_message" => "Ciao",
            "my_catalog_label" => "IL MIO CATALOGO",
            "my_activity_label" => "LA MIA ATTIVITÀ",
            "my_formation_label" => "I MIEI TRAINING",
            "my_training_path_label" => "IL MIO PERCORSO FORMATIVO",
            "see_more_label" => "SCOPRI DI PIÙ",
            "contact_label" => "CONTATTI",
            "link_catalog" => "90/ittraining-catalog",
            "link_my_training_path" => "92/il-mio-percorso-formativo"
        ),
        "tr" => array(
            "hello_message" => "Merhaba",
            "my_catalog_label" => "KATALOĞUM",
            "my_activity_label" => "AKTIVITEM",
            "my_formation_label" => "EĞITIMIM",
            "my_training_path_label" => "EĞITIM YOLCULUĞUM",
            "see_more_label" => "DAHA FAZLA",
            "contact_label" => "BİZE ULAŞIN",
            "link_catalog" => "85/trtraining-catalog",
            "link_my_training_path" => "87/egitim-yolculugum"
        ),
        "hu" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "KATALÓGUS",
            "my_activity_label" => "AKTIVITÁSAIM",
            "my_formation_label" => "TRÉNINGEM",
            "my_training_path_label" => "ELÉRHETŐ TRÉNINGEK",
            "see_more_label" => "TANULJ TÖBBET",
            "contact_label" => "CONTACT",
            "link_catalog" => "80/hutraining-catalog",
            "link_my_training_path" => "82/elerheto-treningek"
        ),
        "pl" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "34/pltraining-catalog",
            "link_my_training_path" => "36/plmy-path"
        ),
        "se" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "40/setraining-catalog",
            "link_my_training_path" => "42/semy-path"
        ),
        "no" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "45/notraining-catalog",
            "link_my_training_path" => "47/nomy-path"
        ),
        "dk" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "50/dktraining-catalog",
            "link_my_training_path" => "52/dkmy-path"
        ),
        "cz" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "55/cztraining-catalog",
            "link_my_training_path" => "57/czmy-path"
        ),
        "de" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "60/detraining-catalog",
            "link_my_training_path" => "62/demy-path"
        ),
        "nl" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "65/nltraining-catalog",
            "link_my_training_path" => "67/nlmy-path"
        ),
        "pt" => array(
            "hello_message" => "Hello",
            "my_catalog_label" => "MY CATALOG",
            "my_activity_label" => "MY ACTIVITY",
            "my_formation_label" => "MY TRAINING",
            "my_training_path_label" => "MY TRAINING PATH", 
            "see_more_label" => "LEARN MORE",
            "contact_label" => "CONTACT",
            "link_catalog" => "75/pttraining-catalog",
            "link_my_training_path" => "77/ptmy-path"
        )
    );
?>
<!DOCTYPE html>
<html lang="<?= $language ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="pragma" content="no-cache" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script>
            $(document).ready(function() {
                $('.carousel').carousel({
                    interval: false
                })
            })            
        </script>
    </head>
    <body>
        <header>
            <?php echo $data[$language]['hello_message'] ?>
            <?= $firstname ? ucwords(strtolower($firstname)) : ($lastname ? ucwords(strtolower($lastname)) : ($email ? $email : $username)) ?>
            <!-- <a href="" style="float: right;"><img src="contact.png" alt=""></a> -->
            <a href="mailto:ks_klepierre_university_hotline@klepierre.com" class="contact">
                <span><?php echo $data[$language]['contact_label'] ?></span>
                <div class="mail">
                    <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 103.02 57.01" style="enable-background:new 0 0 103.02 57.01; fill:none;stroke:#131C4E;stroke-width:3;stroke-miterlimit:10;" xml:space="preserve">
                    <g>
                        <path class="st0" d="M91.49,53.21H11.49c-4.25,0-7.73-3.48-7.73-7.73V10.8c0-4.25,3.48-7.73,7.73-7.73h80.01
                            c4.25,0,7.73,3.48,7.73,7.73v34.69C99.22,49.74,95.74,53.21,91.49,53.21z"/>
                        <path class="st0" d="M98.03,6.69L57.56,37.06c-2.97,2.82-7.83,2.82-10.8,0L4.94,6.69"/>
                    </g>
                    </svg>
                </div>
            </a>
        </header>
        <section class='widget_wrapper'>
                <section class='buttons'>
                    <a href="<?php echo "https://www.university.klepierre.com/pages/".$data[$language]['link_catalog'] ?>" target='_parent'>
                        <div class='button green'>
                            <div class='wrapper'>
                                <img src='images/icon-catalogue.png'>
                                <span><?php echo $data[$language]['my_catalog_label'] ?></span>
                            </div>
                        </div>
                    </a>
                    <a href="https://www.university.klepierre.com/legacy/lms/index.php%3Fr%3DmyActivities/index" target='_parent'>
                        <div class='button orange'>
                            <div class='wrapper'>
                                <img src='images/icon-activite.png'>
                                <span><?php echo $data[$language]['my_activity_label'] ?></span>
                            </div>
                        </div>
                    </a>
                    <a href="https://www.university.klepierre.com/learn/mycourses?mclp0=c-_20" target='_parent'>
                        <div class='button red'>
                            <div class='wrapper'>
                                <img src='images/icon-formation.png'>
                                <span><?php echo $data[$language]['my_formation_label'] ?></span>
                            </div>
                        </div>
                    </a>
                    <a href="<?php echo "https://www.university.klepierre.com/pages/".$data[$language]['link_my_training_path']?>" target='_parent'>
                        <div class='button blue'>
                            <div class='wrapper'>
                                <img src='images/icon-parcours.png'>
                                <span><?php echo $data[$language]['my_training_path_label'] ?></span>
                            </div>
                        </div>
                    </a>
                </section>
                <section class="container">
                    <?php require_once ('carousel.php'); ?>
                </section>
        </section>
    </body>
</html>