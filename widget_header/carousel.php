<?php
    $language = $_GET['lang'];

    $urlBegin = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";

    // $jsonFileLink = "https://remote.yesnyou.com/klepierre/widget-news/server/data/".$language.".json";
    $jsonFileLink = $urlBegin.'klepierre/widget-news/server/data/'.$language.'.json';

    $content = file_get_contents($jsonFileLink);

    if($content == false) {
        echo '{"success": false, "error": "Le fichier n\'existe pas ou est vide."}';
        exit;
    }
    else{
        $CarouselContent = json_decode($content);
        $numberOfSliders = sizeof($CarouselContent);
?>
    <div id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <?php 
                for($i = 0; $i<$numberOfSliders; $i++){
            ?>
                    <li data-target="#myCarousel" data-slide-to=<?php echo $i;?> class="<?php if($i == 0) echo 'active'; else echo '';  ?>"></li>
            <?php
                }
            ?>
        </ol>

        <div class="carousel-inner">
            <?php
                for($i = 0; $i<$numberOfSliders; $i++){
                ?>
                    <div class="item <?php if($i == 0) echo 'active'; else echo '';  ?>" style='background-image: url(<?php echo $CarouselContent[$i]->imageLink; ?>);'>
                        <div class="carousel-caption d-none d-md-block">
                            <?php if($CarouselContent[$i]->pageLink !== ""){ ?>
                                <a href="<?php echo $CarouselContent[$i]->pageLink; ?>" target='_blank'>
                                    <button>VOIR →</button>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
            <?php
                }
            ?>
        </div>

        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php
    }
?>