<?php 
/**
 * Here, PHP will get the user identity by requesting the LMS via CURL
 * The first authentication steps must be code in PHP, because grant_type "authorization_code"
 * can not be fired via AJAX request.
 * And some secret credentials MUST NOT be written in JS sources.
 * 
 * IMPORTANT REQUIREMENT!
 *  "curl" command line must be installed on your machine
 *  (the PHP native curl function is not preferred because of long parameter passed and because PHP curl function
 *  is not so easy to debug compared to command line)
 */

$firstname = $lastname = $email = $username = '';
$language = 'fr';
$code = !empty($_REQUEST['auth_code']) ? $_REQUEST['auth_code'] : null;
if ($code){
    // TODO: set credentials into a setting file when this project will be migrated into the YnY API
    // Get access token, and then request for user session
    $client_id = 'KLEPIERRE_IT';
    $client_secret = '804deb3f4b0ab0792ca1f7961b81f5da741e02a0';
    $redirect_uri = 'http://www.yesnyou.com/_callback';
    $grant_type = 'authorization_code';
    $token_endpoint = 'https://www.university.klepierre.com/oauth2/token';
    // First curl command line to request an access_token
    $curl_cmd = <<<CURL
curl -X POST $token_endpoint -d "grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret&code=$code&redirect_uri=$redirect_uri" 2>/dev/null
CURL;
    $lms_response = exec($curl_cmd);
    if ($lms_response){
        $arrResponse = json_decode($lms_response, true);
        if (!empty($arrResponse['access_token'])){
            // TODO: should store access_token & refresh token into session or cookie (then, access_token could be used by Javascript)
            $access_token = $arrResponse['access_token'];
            $refresh_token = !empty($arrResponse['refresh_token']) ? $arrResponse['refresh_token'] : null;
            $expires_in = !empty($arrResponse['expires_in']) ? $arrResponse['expires_in'] : 3600;//default 1h
            $token_type = !empty($arrResponse['token_type']) ? $arrResponse['token_type'] : 'Bearer';
            $scope = !empty($arrResponse['scope']) ? $arrResponse['scope'] : 'api';
            // Then, get user session
            $api_endpoint = 'https://www.university.klepierre.com/manage/v1/user/session';
            // Second curl command line to request user identity
            $curl_cmd = <<<CURL
curl -X GET $api_endpoint -H 'Authorization: $token_type $access_token' 2>/dev/null
CURL;
            $lms_response = exec($curl_cmd);
            if ($lms_response){
                $arrResponse = json_decode($lms_response, true);
                if (!empty($arrResponse['data']['id'])){
                    // eventually, get the user_id
                    //$user_id = $arrResponse['data']['id']; // But user_id is also passed by the LMS into the iframe URL (such as username)
                    $firstname = trim($arrResponse['data']['firstname']);
                    $lastname = trim($arrResponse['data']['lastname']);
                    $email = trim($arrResponse['data']['email']);
                    $username = trim($arrResponse['data']['username']);
                    $language = $arrResponse['data']['language'];
                }
            }
        }
    }
} ?>