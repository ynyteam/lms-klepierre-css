<?php
    $language = $_GET['lang'];
    $data = array (
        "fr" => array(
            "surveys_label" => "MES ÉVALUATIONS",
            "certificates_label" => "MES ATTESTATIONS"
        ),
        "en" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "es" => array(
            "surveys_label" => "MIS EVALUACIONES",
            "certificates_label" => "MIS FORMACIONES CERTIFICADAS"
        ),
        "it" => array(
            "surveys_label" => "RISULTATI DEI MIEI TRAINING",
            "certificates_label" => "LE MIE CERTIFICAZIONI"
        ),
        "tr" => array(
            "surveys_label" => "EĞITIM ANKETLERIM",
            "certificates_label" => "SERTIFIKALI EĞITIMIM"
        ),
        "hu" => array(
            "surveys_label" => "TRÉNING KÉRDŐÍVEK",
            "certificates_label" => "ELVÉGZETT TRÉNINGEK"
        ),
        "pl" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "se" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "no" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "dk" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "cz" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "de" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "nl" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        ),
        "pt" => array(
            "surveys_label" => "MY TRAINING SURVEYS",
            "certificates_label" => "MY CERTIFICATES"
        )
    );
?>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="cache-control" content="no-cache">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
            <div class='wrapper'>
                    <div class='sub_buttons'>
                        <a style='color: white;' href='https://www.university.klepierre.com/learn/mycourses?mclp0=c-_21' target='_parent'>
                            <div class='button' style='background-color: #32d1cd;'>
                                <div class='wrapper_button'>
                                        <div>
                                            <img style='max-width:70px;' src='https://remote.yesnyou.com/klepierre/lms-klepierre-css/widget_tableau_de_bord/images/icon-evaluation_blanc.png'>
                                        </div>
                                        <div>
                                            <p style='font-size: 14px; margin-top:35px; margin-bottom: 0px;'><?php echo $data[$language][surveys_label]; ?></p>
                                        </div>
                                </div>
                            </div>
                        </a>
                        <a style='color: white;'  href='https://www.university.klepierre.com/learn/mycourses?mclp0=s-_completed~c-_22' target='_parent'>
                            <div class='button' style='background-color: #71d1df;'>
                                <div class='wrapper_button'>
                                        <div>
                                            <img style='max-width:75px;' src='https://remote.yesnyou.com/klepierre/lms-klepierre-css/widget_tableau_de_bord/images/icon-attestation_blanc.png'>
                                        </div>
                                        <div>
                                            <p style='font-size: 14px; margin-top:35px; margin-bottom: 0px;'><?php echo $data[$language][certificates_label]; ?></p>
                                        </div>
                                </div>
                            </div>
                        </a>
                    </div>
            </div>
    </body>
    
</html>

