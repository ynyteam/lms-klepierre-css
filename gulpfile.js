'use strict'

var postcss = require('gulp-postcss')
var autoprefixer = require('autoprefixer')
var cssnext = require('cssnext')
var precss = require('precss')

var myplugin = require('postcss-importantly')

var gulp = require('gulp')
var sass = require('gulp-sass')
// var sourcemaps = require('gulp-sourcemaps')

var processors = [
  autoprefixer,
  cssnext,
  precss
]
var buildProc = [
  myplugin
]

gulp.task('css', function () {
  return gulp.src('./src/*.css')
    .pipe(postcss(processors))
    .pipe(gulp.dest('./dest'))
})

gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(postcss(buildProc))
    .pipe(gulp.dest('./css'))
})

gulp.task('build', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(postcss(buildProc))
    .pipe(gulp.dest('./css/scssOut'))
})

gulp.task('sass:watch', function () {
  gulp.watch('./scss/**/*.scss', ['sass'])
})

// gulp.task('sass', function () {
//   return gulp.src('./scss/**/*.scss')
//     .pipe(sourcemaps.init())
//     .pipe(sass().on('error', sass.logError))
//     .pipe(sourcemaps.write())
//     .pipe(gulp.dest('./css/scssOut'))
// })
